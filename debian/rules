#!/usr/bin/make -f

PY2VERS := $(shell pyversions -s)
PY3VERS := $(shell py3versions -s)

export CFLAGS = $(shell dpkg-buildflags --get CFLAGS)
export CPPFLAGS = $(shell dpkg-buildflags --get CPPFLAGS)
export LDFLAGS = $(shell dpkg-buildflags --get LDFLAGS)

%:
	dh $@ --with python2,python3,sphinxdoc

override_dh_auto_build:
	set -e ; \
	for python in $(PY2VERS) $(PY3VERS); do \
		$$python setup.py build; \
	done
	$(MAKE) -C docs html

override_dh_auto_install:
	set -e ; \
	for python in $(PY2VERS); do \
		$$python setup.py install --install-layout=deb --root $(CURDIR)/debian/python-psutil; \
	done
	set -e ; \
	for python in $(PY3VERS); do \
		$$python setup.py install --install-layout=deb --root $(CURDIR)/debian/python3-psutil; \
	done

override_dh_auto_test:
ifeq ($(filter nocheck,$(DEB_BUILD_OPTIONS)),)
	# run tests
	-for test in test_memory_leaks.py test_psutil.py ;\
	do \
	    for python in $(PY2VERS) ; do \
  	        echo "running "test/$$test" on "$$python ; \
		LIB=$$($$python -c "from distutils.command.build import build ; from distutils.core import Distribution ; b = build(Distribution()) ; b.finalize_options() ; print b.build_platlib") ; \
	        PYTHONPATH=$$LIB $$python test/$$test ; \
	    done ; \
	    for python in $(PY3VERS) ; do \
  	        echo "running "test/$$test" on "$$python ; \
		LIB=$$($$python -c "from distutils.command.build import build ; from distutils.core import Distribution ; b = build(Distribution()) ; b.finalize_options() ; print (b.build_platlib)") ; \
	        PYTHONPATH=$$LIB $$python test/$$test ; \
	    done ; \
	done
endif

override_dh_installchangelogs:
	dh_installchangelogs HISTORY

override_dh_installdocs:
	dh_installdocs -X.DS_Store
	dh_installdocs docs/_build/html

override_dh_compress:
	dh_compress -X.py
